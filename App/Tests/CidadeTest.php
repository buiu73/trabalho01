<?php
use Livro\Database\Transaction;
use Livro\Database\Repository;
use Livro\Database\Criteria;

use PHPUnit\Framework\TestCase;

class CidadeTest extends TestCase
{
    public function testGetEstado()
    {
        Transaction::open('livro');
	$id_cidade = 1;
        $cidade = new Cidade($id_cidade);
        

        $this->assertEquals( $cidade->nome, "Aracajú");
        //Transaction::close();
    }
}
