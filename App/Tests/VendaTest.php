<?php
use Livro\Database\Transaction;
use Livro\Database\Repository;
use Livro\Database\Criteria;

use PHPUnit\Framework\TestCase;

class VendaTest extends TestCase
{
    public function testDescontoVenda()
    {
        Transaction::open('livro');
	
        $teste = 0;
	$valor_test = 20;

        $pessoa = new Pessoa;
        $pessoa->nome = 'Ferdinando';
        $pessoa->store();

        $venda = new Venda;
        $venda->data_venda  = date('Y-m-d');
	$venda->cliente = $pessoa;
        $venda->desconto = 20;

        $produtos = [1];
        $total_produtos = 0;

        foreach ($produtos as $id)
        {
            $venda->addItem($produto = new Produto($id), 1);
            $total_produtos += $produto->preco_venda;
        }

        $venda->valor_venda = $total_produtos;
        $venda->valor_final = $total_produtos - $venda->desconto;
        $venda->store();
        
        $this->assertEquals( $venda->valor_final, $valor_test);
    }
}
